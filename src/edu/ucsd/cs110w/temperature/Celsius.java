/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author haperez
 *
 */
public class Celsius extends Temperature{
	public Celsius(float t)
	{
	super(t);
	} public String toString()
	{
	// TODO: Complete this method 
		return String.valueOf(getValue())+" C";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit((getValue()*(float)9/5)+32);
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return new Kelvin((getValue()+273));
	}
}
