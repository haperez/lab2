/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (haperez): write class javadoc
 *
 * @author haperez
 *
 */
public class Kelvin extends Temperature {
	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return String.valueOf(getValue()) + " K";
	}
	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toCelsius()
	 */
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return new Celsius(getValue()-273);
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toFahrenheit()
	 */
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit((getValue()-273)*((float)9/5) + 32);
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return this;
	}

}
